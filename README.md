# Simple Web Service Application

**This Sample RESTful web service is created with Spring Boot Applications**

Application operable on web-server API (Rest API) using springboot

Implementation details:

1. A simple root endpont which accept HTTP GET request

    http://localhost:8080/

and responds  back with a String 

    Hello World

2. Metadata endpoint which accept HTTP GET request

    http://localhost:8080/app-details

and responds back with basic information of the application in JSON format represtation 

    {"name":"application java","version":"v0.1.0","description":"Pre-interview-codeTest"}

Customize the app-details with an optional name parameter in the query string

    http://localhost:8080/app-details?name=simplewebapp

The name parameter value overrides the default value of "java" and is reflected in the response:

    {"name":"application simplewebapp","version":"v0.1.0","description":"Pre-interview-codeTest"}

3. The Health endpoint returns the status

    http://localhost:8080/actuator/health

    {"status":"UP"}

# Prerequisites 

- JDK 1.8 or later
- Maven 3.2+
- Favorite Text Editor

# Build with Maven

Directory Structure - **src/main/java/hello**

Build system is Maven Wrapper, Spring Boot Maven Plugin to build apps with Spring. The scripts are placed under the root folder

**pom.xml** - Maven projects are defined with an XML file, this file gives the projects name,version and dependencies that is on external libraries


# Resource Representation Class and Resource controller

**SampleResponse.java** models the app-details represenatation with plain old java object with fields, constructors and accessors for the name, version and description data.

**ResponseController.java** handles the HTTP requests and identified by the @RestController annotation.

*ResponseController class* handles /GET request for /app-details by returing new value of the SampleResponse class

- @RequestMapping ensures HTTP requests are mapped to the SampleResponse() method.

- @RequestParam binds the value od the query string parameter 'name' into the 'name' parameter of the SampleResponse () method, If the paramater is absent the defaultValue is used.

# Make the application executable and Build an executable JAR

**MyApplication.java** - creates a standalone application by package everything in a single, executable JAR file 	

Run the application using Maven

    mvnw spring-boot:run 

**Build an executable jar **

    mvnw clean package

    java -jar target/myapp-rest-service-0.1.0.jar


# T E S T S with Junit

Spring Boot provides a number of utilities and annotations to help test a Spring Boot Application.

**pom.xml** is updated with the dependency 'spring-boot-starter-test'

**MyAppTest.java**:

@SpringBootTest annotation works by creating ApplicationContext used in your tests via SpringApplication.

@Autowired  annotation is interpreted by the Spring and the controller is injected before the test methods are run.

MockMvc - injected using @AutoConfigureMockMvc annotation, processes the HTTP request without starting the server.


# CI/CD with GitLab

**.gitlab-ci.yml** - config file for GitLab to run the set of commands whenever the code is pushed to the repository to build and deploy the application.

**stage: build** 

Docker image java:8 is build to build the application and "mvn package" generates the artifcat under the target folder.

**stage: deploy** 

Continuous deployment would be good option to be demonstrated.

- An account was created on a CF instance (In this case its IBM cloud)

- CF_USERNAME and CF_PASSWORD provided as environment variables on GitLab CI/CD - Settings > CI/CD
 
- metadata.yml:  Is the configuration for the CF CLI we will use to deploy the application

- Deploy script will deploy the application to PWS at every push to teh master branch

- Once the app is deployment is successful it will display the route (which is the URL) of the application

    *For example:*

    .............
    
    Waiting for app to start...
    name:              simplewebservice-myapp
    requested state:   started
    routes:            simplewebservice-myapp-surprised-pangolin-lx.mybluemix.net
    last uploaded:     Sat 14 Nov 14:10:25 UTC 2020
    
    .............
    
    
    *Response*:

    https://simplewebservice-myapp-surprised-pangolin-lx.mybluemix.net/

    Hello World
    
    https://simplewebservice-myapp-surprised-pangolin-lx.mybluemix.net/actuator/health

    {"status":"UP"}

    https://simplewebservice-myapp-surprised-pangolin-lx.mybluemix.net/app-details

    {"name":"application java","version":"v0.1.0","description":"Pre-interview-codeTest"}
    
    https://simplewebservice-myapp-surprised-pangolin-lx.mybluemix.net/app-details?name=simplewebapp

    {"name":"application simplewebapp","version":"v0.1.0","description":"Pre-interview-codeTest"}

# Conclusion

- **myapp-rest-service-0.1.0.jar** is generated as the single deployable artifact

- When the build is completed with gitlab pipeline the artifact is available for download and can be deployed.

- Or Gitlab CI/CD to build and Deploy the application to Cloud Foundary Instance.

There are no identified risks with the application or deployment. There are dependencies on Java and the Spring root libraries to successfuly Build and Deploy this sample web service application.
The response and health endpoints can be customised as per the requirements.



    ** References used write this application **:

    https://spring.io/guides/gs/rest-service/

    https://spring.io/guides/gs/testing-web/

    https://spring.io/guides/gs/actuator-service/
    
    https://docs.gitlab.com/ee/ci/examples/deploy_spring_boot_to_cloud_foundry/
